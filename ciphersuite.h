#ifndef CIPHERSUITE_H
#define CIPHERSUITE_H

#include <QObject>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <linux/if_alg.h>
#include <linux/socket.h>
#include <errno.h>
#include <QDebug>

#define SHA224_DIGEST_SZ 28
#define SHA256_DIGEST_SZ 32
#define SHA384_DIGEST_SZ 48
#define SHA512_DIGEST_SZ 64
#define AES_DIGEST_SZ    16


class CipherSuite : public QObject
{
    Q_OBJECT
public:
    explicit CipherSuite(QObject *parent = nullptr);
    ~CipherSuite();
    QByteArray Sha224(QString text);
    QByteArray Sha256(QString text);
    QByteArray Sha384(QString text);
    QByteArray Sha512(QString text);
    QByteArray Hmac_Sha224(QString key, QString text);
    QByteArray Hmac_Sha256(QString key, QString text);
    QByteArray Hmac_Sha384(QString key, QString text);
    QByteArray Hmac_Sha512(QString key, QString text);
    QByteArray AES_CBC_Encrypt(QString key, QString iv, QString text);
    QByteArray AES_CBC_Decrypt(QString key, QString iv, QByteArray text);


signals:

public slots:

private:
    enum Function{
        SHA224,
        SHA256,
        SHA384,
        SHA512,
        HMAC_SHA224,
        HMAC_SHA256,
        HMAC_SHA384,
        HMAC_SHA512,
        AES_CBC_ENCRYPT,
        AES_CBC_DECRYPT
    } ;

    QList<ssize_t> digest_size =
    {
        SHA224_DIGEST_SZ,
        SHA256_DIGEST_SZ,
        SHA384_DIGEST_SZ,
        SHA512_DIGEST_SZ,
        SHA224_DIGEST_SZ,
        SHA256_DIGEST_SZ,
        SHA384_DIGEST_SZ,
        SHA512_DIGEST_SZ,
        AES_DIGEST_SZ,
        AES_DIGEST_SZ

    };

    QByteArray cipherFunc(QString key, QVariant text, QString IV, Function fc );


    int sock_sha224;
    int sock_sha256;
    int sock_sha384;
    int sock_sha512;

};

#endif // CIPHERSUITE_H
