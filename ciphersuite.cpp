#include "ciphersuite.h"

CipherSuite::CipherSuite(QObject *parent) : QObject(parent)
{
    struct sockaddr_alg sa;
    int fd;


    memset(&sa,0,sizeof(sa));
    sa.salg_family = AF_ALG;
    memcpy(sa.salg_type,"hash",5);
    memcpy(sa.salg_name,"sha512",6);
    fd = socket(AF_ALG, SOCK_SEQPACKET, 0);
    bind(fd, (struct sockaddr *)&sa, sizeof(sa));
    sock_sha512 = accept(fd, NULL, 0);
    close(fd);

    memset(&sa,0,sizeof(sa));
    sa.salg_family = AF_ALG;
    memcpy(sa.salg_type,"hash",5);
    memcpy(sa.salg_name,"sha256",6);
    fd = socket(AF_ALG, SOCK_SEQPACKET, 0);
    bind(fd, (struct sockaddr *)&sa, sizeof(sa));
    sock_sha256 = accept(fd, NULL, 0);
    close(fd);

    memset(&sa,0,sizeof(sa));
    sa.salg_family = AF_ALG;
    memcpy(sa.salg_type,"hash",5);
    memcpy(sa.salg_name,"sha224",6);
    fd = socket(AF_ALG, SOCK_SEQPACKET, 0);
    bind(fd, (struct sockaddr *)&sa, sizeof(sa));
    sock_sha224 = accept(fd, NULL, 0);
    close(fd);


    memset(&sa,0,sizeof(sa));
    sa.salg_family = AF_ALG;
    memcpy(sa.salg_type,"hash",5);
    memcpy(sa.salg_name,"sha384",6);
    fd = socket(AF_ALG, SOCK_SEQPACKET, 0);
    bind(fd, (struct sockaddr *)&sa, sizeof(sa));
    sock_sha384 = accept(fd, NULL, 0);
    close(fd);

//    QString key = "0000000000000000";
//    QString IV =  "0000000000000000";
//    QString text = "0000000000000000";
//    int opfd;
//        int tfmfd;
//        struct sockaddr_alg sa1 = { };
//        memcpy(sa1.salg_type,"skcipher",8);
//        memcpy(sa1.salg_name,"cbc(aes)",8);
//        sa1.salg_family = AF_ALG;

//        struct msghdr msg = {};
//        struct cmsghdr *cmsg;
//        char cbuf[CMSG_SPACE(4) + CMSG_SPACE(20)]__attribute__((aligned(4)));
//        memset(cbuf,0,sizeof(cbuf));
//        char buf[16];
//        struct af_alg_iv *iv;
//        struct iovec iov;
//        memset(&iov,0,sizeof(iov));
//        int i;

//        tfmfd = socket(AF_ALG, SOCK_SEQPACKET, 0);

//        bind(tfmfd, (struct sockaddr *)&sa1, sizeof(sa1));

//        setsockopt(tfmfd, SOL_ALG, ALG_SET_KEY, key.toUtf8().data(), 16);

//        opfd = accept(tfmfd, NULL, 0);

//        msg.msg_control = cbuf;
//        msg.msg_controllen = sizeof(cbuf);

//        cmsg = CMSG_FIRSTHDR(&msg);
//        cmsg->cmsg_level = SOL_ALG;
//        cmsg->cmsg_type = ALG_SET_OP;
//        cmsg->cmsg_len = CMSG_LEN(4);
//        *(__u32 *)CMSG_DATA(cmsg) = ALG_OP_ENCRYPT;

//        cmsg = CMSG_NXTHDR(&msg, cmsg);
//        cmsg->cmsg_level = SOL_ALG;
//        cmsg->cmsg_type = ALG_SET_IV;
//        cmsg->cmsg_len = CMSG_LEN(20);
//        iv = (struct af_alg_iv *)CMSG_DATA(cmsg);
//        iv->ivlen = 16;
//        memcpy(iv->iv, IV.toUtf8().data(), 16);

//        iov.iov_base = text.toUtf8().data();
//        iov.iov_len = 16;

//        msg.msg_iov = &iov;
//        msg.msg_iovlen = 1;

//        int rer=sendmsg(opfd, &msg, 0);
//        rer=read(opfd, buf, 16);

//        for (i = 0; i < 16; i++) {
//            printf("%02x", (unsigned char)buf[i]);
//        }
//        printf("\n");

//        close(opfd);
//        close(tfmfd);

}

CipherSuite::~CipherSuite()
{
    close(sock_sha512);
    close(sock_sha384);
    close(sock_sha256);
    close(sock_sha224);

}

QByteArray CipherSuite::cipherFunc(QString key, QVariant text, QString IV, Function fc )
{
    ssize_t len =digest_size[fc];
    char digest[len];

    bool toClose =false;
    bool useWrite = true;
    QByteArray retQBA;
    struct msghdr msg = {};


    memset(digest,0,len);
    retQBA.clear();

    int _socket;
    switch (fc) {

    case SHA256:
    {
        _socket=sock_sha256;
    }
    break;


    case SHA512:
    {
         _socket=sock_sha512;
    }
    break;

    case SHA224:
    {
         _socket=sock_sha224;
    }
    break;

    case SHA384:
    {
         _socket=sock_sha384;
    }
    break;

    case HMAC_SHA224:
    {
         struct sockaddr_alg sa;
         memset(&sa,0,sizeof(sa));
         sa.salg_family = AF_ALG;
         memcpy(sa.salg_type,"hash",5);
         memcpy(sa.salg_name,"hmac(sha224)",sizeof("hmac(sha224)"));
        int  fd = socket(AF_ALG, SOCK_SEQPACKET, 0);
        bind(fd, (struct sockaddr *)&sa, sizeof(sa));
        setsockopt(fd,SOL_ALG,ALG_SET_KEY, key.toStdString().c_str(), key.length());
        _socket = accept(fd, NULL, 0);
        close(fd);
         toClose =true;
    }
    break;

    case HMAC_SHA256:
    {
         struct sockaddr_alg sa;
         memset(&sa,0,sizeof(sa));
         sa.salg_family = AF_ALG;
         memcpy(sa.salg_type,"hash",5);
         memcpy(sa.salg_name,"hmac(sha256)",sizeof("hmac(sha256)"));
        int  fd = socket(AF_ALG, SOCK_SEQPACKET, 0);
        bind(fd, (struct sockaddr *)&sa, sizeof(sa));
        setsockopt(fd,SOL_ALG,ALG_SET_KEY, key.toStdString().c_str(), key.length());
        _socket = accept(fd, NULL, 0);
        close(fd);
         toClose =true;
    }
    break;

    case HMAC_SHA384:
    {
         struct sockaddr_alg sa;
         memset(&sa,0,sizeof(sa));
         sa.salg_family = AF_ALG;
         memcpy(sa.salg_type,"hash",5);
         memcpy(sa.salg_name,"hmac(sha384)",sizeof("hmac(sha384)"));
        int  fd = socket(AF_ALG, SOCK_SEQPACKET, 0);
        bind(fd, (struct sockaddr *)&sa, sizeof(sa));
        setsockopt(fd,SOL_ALG,ALG_SET_KEY, key.toStdString().c_str(), key.length());
        _socket = accept(fd, NULL, 0);
        close(fd);
         toClose =true;
    }
    break;

    case HMAC_SHA512:
    {
         struct sockaddr_alg sa;
         memset(&sa,0,sizeof(sa));
         sa.salg_family = AF_ALG;
         memcpy(sa.salg_type,"hash",5);
         memcpy(sa.salg_name,"hmac(sha512)",sizeof("hmac(sha512)"));
        int  fd = socket(AF_ALG, SOCK_SEQPACKET, 0);
        bind(fd, (struct sockaddr *)&sa, sizeof(sa));
        setsockopt(fd,SOL_ALG,ALG_SET_KEY, key.toStdString().c_str(), key.length());
        _socket = accept(fd, NULL, 0);
        close(fd);
         toClose =true;
    }
    break;

    case AES_CBC_ENCRYPT :
    {
        struct sockaddr_alg sa;
        memset(&sa,0,sizeof(sa));
        int res =0;
        int tfmfd;
        sa.salg_family = AF_ALG;
        memcpy(sa.salg_type,"skcipher",8);
        memcpy(sa.salg_name,"cbc(aes)",8);

        memset(&msg,0,sizeof(msg));
        struct cmsghdr *cmsg;
        char cbuf[CMSG_SPACE(4) + CMSG_SPACE(20)];
        memset(cbuf,0,sizeof (cbuf));
        struct af_alg_iv *iv_s;
        struct iovec iov;

        tfmfd = socket(AF_ALG, SOCK_SEQPACKET, 0);
        res =bind(tfmfd, (struct sockaddr *)&sa, sizeof(sa));
        res =setsockopt(tfmfd, SOL_ALG, ALG_SET_KEY, key.toUtf8().data(), key.length());
        _socket = accept(tfmfd, NULL, 0);

        msg.msg_control = cbuf;
        msg.msg_controllen = sizeof(cbuf);

        cmsg = CMSG_FIRSTHDR(&msg);
        cmsg->cmsg_level = SOL_ALG;
        cmsg->cmsg_type = ALG_SET_OP;
        cmsg->cmsg_len = CMSG_LEN(4);
        *(__u32 *)CMSG_DATA(cmsg) = ALG_OP_ENCRYPT;

        cmsg = CMSG_NXTHDR(&msg, cmsg);
        cmsg->cmsg_level = SOL_ALG;
        cmsg->cmsg_type = ALG_SET_IV;
        cmsg->cmsg_len = CMSG_LEN(20);
        iv_s = (struct af_alg_iv *)CMSG_DATA(cmsg);
        iv_s->ivlen = 16;
        memcpy(iv_s->iv, IV.toUtf8().data(), IV.length());

        QString txt = text.toString();
        iov.iov_base = (void *)txt.toUtf8().data();
        int add =0;
        if(txt.length()%16!=0)
            add=1;

        iov.iov_len = (text.toString().length()/16+add)*16;

        msg.msg_iov = &iov;
        msg.msg_iovlen = 1;
        close(tfmfd);



        toClose=true;
        useWrite=false;
    }
    break;

    case AES_CBC_DECRYPT :
    {

        struct sockaddr_alg sa;
        memset(&sa,0,sizeof(sa));
        int res =0;
        int tfmfd;
        sa.salg_family = AF_ALG;
        memcpy(sa.salg_type,"skcipher",8);
        memcpy(sa.salg_name,"cbc(aes)",8);

        memset(&msg,0,sizeof(msg));
        struct cmsghdr *cmsg;
        char cbuf[CMSG_SPACE(4) + CMSG_SPACE(20)];
        memset(cbuf,0,sizeof (cbuf));
        struct af_alg_iv *iv_s;
        struct iovec iov;

        tfmfd = socket(AF_ALG, SOCK_SEQPACKET, 0);
        res =bind(tfmfd, (struct sockaddr *)&sa, sizeof(sa));
        res =setsockopt(tfmfd, SOL_ALG, ALG_SET_KEY, key.toUtf8().data(), key.length());
        _socket = accept(tfmfd, NULL, 0);

        msg.msg_control = cbuf;
        msg.msg_controllen = sizeof(cbuf);

        cmsg = CMSG_FIRSTHDR(&msg);
        cmsg->cmsg_level = SOL_ALG;
        cmsg->cmsg_type = ALG_SET_OP;
        cmsg->cmsg_len = CMSG_LEN(4);
        *(__u32 *)CMSG_DATA(cmsg) = ALG_OP_DECRYPT;

        cmsg = CMSG_NXTHDR(&msg, cmsg);
        cmsg->cmsg_level = SOL_ALG;
        cmsg->cmsg_type = ALG_SET_IV;
        cmsg->cmsg_len = CMSG_LEN(20);
        iv_s = (struct af_alg_iv *)CMSG_DATA(cmsg);
        iv_s->ivlen = 16;
        memcpy(iv_s->iv, IV.toUtf8().data(), IV.length());

        iov.iov_base = (void *)text.toByteArray().data();
        iov.iov_len = text.toByteArray().length()/16*16;
        msg.msg_iov = &iov;
        msg.msg_iovlen = 1;
        close(tfmfd);


        toClose=true;
        useWrite=false;
        toClose=true;
        useWrite=false;
    }
    break;

    }


    if(useWrite)
    {
        QString txt =text.toString();
        write(_socket, txt.toStdString().c_str(), txt.length());
        read(_socket, digest, len);
        retQBA.append(digest);
    }
    else
    {
        int r =sendmsg(_socket, &msg, 0);
        while((r=read(_socket, digest, len))>0)
        {
            retQBA.append(digest,r);
        }



    }


    if(toClose) close(_socket);

    return retQBA;
}

QByteArray CipherSuite::Sha256(QString text)
{
    return cipherFunc("",text,"", SHA256);
}

QByteArray CipherSuite::Sha384(QString text)
{
    return cipherFunc("",text,"", SHA384);
}

QByteArray CipherSuite::Sha224(QString text)
{
     return cipherFunc("",text,"", SHA224);
}

QByteArray CipherSuite::Sha512(QString text)
{
    return cipherFunc("",text,"", SHA512);
}

QByteArray CipherSuite::Hmac_Sha224(QString key, QString text)
{
    if(key.length()==0 )
        return QByteArray("Invalid Key or IV");

     return cipherFunc(key,text,"", HMAC_SHA224);
}

QByteArray CipherSuite::Hmac_Sha256(QString key, QString text)
{
    if(key.length()==0 )
        return QByteArray("Invalid Key or IV");

    return cipherFunc(key,text,"", HMAC_SHA256);
}

QByteArray CipherSuite::Hmac_Sha384(QString key, QString text)
{
    if(key.length()==0 )
        return QByteArray("Invalid Key or IV");

     return cipherFunc(key,text,"", HMAC_SHA384);
}

QByteArray CipherSuite::Hmac_Sha512(QString key, QString text)
{
    if(key.length()==0 )
        return QByteArray("Invalid Key or IV");
    return cipherFunc(key,text,"", HMAC_SHA512);
}

QByteArray CipherSuite::AES_CBC_Encrypt(QString key, QString iv, QString text)
{
    if((key.length()!=32 &&
            key.length()!=24 &&
            key.length()!=16) ||
            iv.length()!=16)
   return QByteArray("Invalid Key or IV");

   return cipherFunc(key,text,iv, AES_CBC_ENCRYPT);
}

QByteArray CipherSuite::AES_CBC_Decrypt(QString key, QString iv, QByteArray text)
{
    if((key.length()!=32 &&
            key.length()!=24 &&
            key.length()!=16) ||
            iv.length()!=16)
    return QByteArray("Invalid Key or Iv");

    return cipherFunc(key,text,iv, AES_CBC_DECRYPT);
}
