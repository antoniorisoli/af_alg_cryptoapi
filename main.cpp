#include <stdio.h>
#include "ciphersuite.h"
#include <QDebug>
int main(void)
{

CipherSuite x;
qDebug()<<"SHA512"<<x.Sha512("Hello").toHex();
qDebug()<<"SHA256"<<x.Sha256("Hello").toHex();
qDebug()<<"SHA384"<<x.Sha384("Hello").toHex();
qDebug()<<"SHA224"<<x.Sha224("Hello").toHex();
qDebug()<<"HMAC224"<<x.Hmac_Sha224("123","Hello").toHex();
qDebug()<<"HMAC256"<<x.Hmac_Sha256("123","Hello").toHex();
qDebug()<<"HMAC384"<<x.Hmac_Sha384("123","Hello").toHex();
qDebug()<<"HMAC512"<<x.Hmac_Sha512("123","Hello").toHex();

QByteArray xx =x.AES_CBC_Encrypt("0000000000000000", "0000000000000000","Using key 128 bit (16B)");
qDebug()<<"AES_CBC_EN"<<xx.toHex();
xx =x.AES_CBC_Decrypt("0000000000000000", "0000000000000000",xx);
qDebug()<<"AES_CBC_DEC"<<QString(xx);

xx =x.AES_CBC_Encrypt("000000000000000000000000", "0000000000000000","Using key 192 bit (24B)");
qDebug()<<"AES_CBC_EN"<<xx.toHex();
xx =x.AES_CBC_Decrypt("000000000000000000000000", "0000000000000000",xx);
qDebug()<<"AES_CBC_DEC"<<QString(xx);

xx =x.AES_CBC_Encrypt("00000000000000000000000000000000", "0000000000000000","Using key 256 bit (32B)");
qDebug()<<"AES_CBC_EN"<<xx.toHex();
xx =x.AES_CBC_Decrypt("00000000000000000000000000000000", "0000000000000000",xx);
qDebug()<<"AES_CBC_DEC"<<QString(xx);
return 0;
}
